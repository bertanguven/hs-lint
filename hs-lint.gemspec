require_relative 'lib/hs/lint/version'

Gem::Specification.new do |spec|
  spec.name          = "hs-lint"
  spec.version       = Hs::Lint::VERSION
  spec.authors       = ["Bertan Guven"]
  spec.email         = ["bertan@homestars.com"]

  spec.summary       = %q{standard ruby lint guide}
  spec.description   = %q{standard ruby lint guide}
  spec.homepage      = "http://example.com"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["allowed_push_host"] = "http://example.com"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "http://example.com"
  spec.metadata["changelog_uri"] = "http://example.com"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'rubocop', '> 0.79.0'
  spec.add_dependency 'rubocop-rails', '~> 2.4'
  spec.add_dependency 'rubocop-performance', '~> 1.5'
  spec.add_dependency 'rubocop-rspec', '~> 1.37'
end
